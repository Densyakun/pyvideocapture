import cv2
import pyautogui
import time

camera_id = 1


def cam_init():
    cap = cv2.VideoCapture(camera_id)
    if cap.isOpened():
        w, h = pyautogui.size()
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, w)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, h)

        return cap
    return None


def start_rec():
    cap = cam_init()
    if cap:
        t = time.time()
        n = 0
        for _ in range(180):
            ret, frame = cap.read()
            if not ret:
                break
            n += 1

            # print(frame.shape)
        if time.time() != t:
            print(int(n / (time.time() - t)))
    cap.release()


if __name__ == "__main__":
    start_rec()
